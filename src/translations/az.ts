export const az: Record<string, string> = {
  'menu.restart': 'Yenidən başlamaq',
  'menu.use-light-theme': 'Qaranlıq Temadan istifadə edin',
  'menu.use-dark-theme': 'İşıqlı Temadan istifadə edin',
  'menu.scores': 'Xallar',
  'menu.about': 'Tətbiq haqqında',

  'language-selector.locale.en': 'English',
  'language-selector.locale.de': 'Deutsch',
  'language-selector.locale.ru': 'Русский',
  'language-selector.locale.az': 'Azərbaycanca',

  'scores.no-scores': 'Hələ heç bir xal yoxdur',

  'about.app-name': '2048',
  'about.app-name.label': 'Tətbiqin adı',
  'about.version.label': 'Versiya',
  'about.build-timestamp.label': 'Qurulma tarixi və vaxtı',
  'about.responsible-developer': 'Rəşad Əsgərbəyli',
  'about.responsible-developer.label': 'Proqram tərtibatçısı',
  'about.website.label': 'Veb səhifə'
};
