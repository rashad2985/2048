export const ru: Record<string, string> = {
  'menu.restart': 'Начать заново',
  'menu.use-light-theme': 'Использовать темную тему',
  'menu.use-dark-theme': 'Использовать светлую тему',
  'menu.scores': 'Результаты',
  'menu.about': 'О приложении',

  'language-selector.locale.en': 'English',
  'language-selector.locale.de': 'Deutsch',
  'language-selector.locale.ru': 'Русский',
  'language-selector.locale.az': 'Azərbaycanca',

  'scores.no-scores': 'Пока нет результатов',

  'about.app-name': '2048',
  'about.app-name.label': 'Название приложения',
  'about.version.label': 'Версия',
  'about.build-timestamp.label': 'Дата и время сборки',
  'about.responsible-developer': 'Рашад Асгарбайли',
  'about.responsible-developer.label': 'Разработчик',
  'about.website.label': 'Веб-сайт'
};
