export const en: Record<string, string> = {
  'menu.restart': 'Restart',
  'menu.use-light-theme': 'Use dark theme',
  'menu.use-dark-theme': 'Use light theme',
  'menu.change-language': 'Change language',
  'menu.scores': 'Scores',
  'menu.about': 'About',

  'language-selector.locale.en': 'English',
  'language-selector.locale.de': 'Deutsch',
  'language-selector.locale.ru': 'Русский',
  'language-selector.locale.az': 'Azərbaycanca',

  'scores.no-scores': 'No scores yet',

  'about.app-name': '2048',
  'about.app-name.label': 'App name',
  'about.version.label': 'Version',
  'about.build-timestamp.label': 'Build date & time',
  'about.responsible-developer': 'Rashad Asgarbayli',
  'about.responsible-developer.label': 'Responsible developer',
  'about.website.label': 'Website'
};
