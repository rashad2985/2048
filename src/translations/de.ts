export const de: Record<string, string> = {
  'menu.restart': 'Neu starten',
  'menu.use-light-theme': 'Dunkles Thema verwenden',
  'menu.use-dark-theme': 'Helles Thema verwenden',
  'menu.scores': 'Spielergebnisse',
  'menu.about': 'Über die App',

  'language-selector.locale.en': 'English',
  'language-selector.locale.de': 'Deutsch',
  'language-selector.locale.ru': 'Русский',
  'language-selector.locale.az': 'Azərbaycanca',

  'scores.no-scores': 'Noch keine Spielergebnisse',

  'about.app-name': '2048',
  'about.app-name.label': 'App-Name',
  'about.version.label': 'Version',
  'about.build-timestamp.label': 'Erstellungsdatum & -uhrzeit',
  'about.responsible-developer': 'Raschad Asgarbayli',
  'about.responsible-developer.label': 'Entwickler',
  'about.website.label': 'Webseite'
};
