import { Box, darken, styled, useMediaQuery, useTheme } from '@mui/material';
import * as React from 'react';
import invariant from 'tiny-invariant';
import { usePrevProps } from '../hooks';
import { pixelSize } from '../styles';

const tileMargin = 2 * pixelSize;

const tileWidthMultiplier = 12.5;

const tileWidth = tileWidthMultiplier * pixelSize;

export const tileTotalWidth = tileWidth + tileMargin;

const StyledTile = styled(Box)<Pick<TileProps, 'value'>>(({ theme, value }) => ({
  position: 'absolute',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  border: 'none',
  margin: 0,
  padding: theme.spacing(1),
  boxSizing: 'border-box',
  borderRadius: theme.spacing(1.5),
  fontWeight: 'bold',
  transitionProperty: 'left, top, transform',
  transitionDuration: '250ms, 250ms, 100ms',
  transform: 'scale(1)',
  '@media (orientation: portrait)': {
    minWidth: '20vw',
    width: '20vw',
    maxWidth: '20vw',
    minHeight: '20vw',
    height: '20vw',
    maxHeight: '20vw',
    fontSize: (value > 100000 && '5vw') || (value > 10000 && '6vw') || (value > 1000 && '8vw') || '10vw'
  },
  '@media (orientation: landscape)': {
    minWidth: `calc((86vh - ${theme.spacing(12)}) / 4)`,
    width: `calc((86vh - ${theme.spacing(12)}) / 4)`,
    maxWidth: `calc((86vh - ${theme.spacing(12)}) / 4)`,
    minHeight: `calc((86vh - ${theme.spacing(12)}) / 4)`,
    height: `calc((86vh - ${theme.spacing(12)}) / 4)`,
    maxHeight: `calc((86vh - ${theme.spacing(12)}) / 4)`,
    fontSize:
      (value > 100000 && `calc((50vh - ${theme.spacing(12)}) / 8)`) ||
      (value > 10000 && `calc((60vh - ${theme.spacing(12)}) / 8)`) ||
      (value > 1000 && `calc((70vh - ${theme.spacing(12)}) / 8)`) ||
      `calc((80vh - ${theme.spacing(12)}) / 8)`
  }
}));

export type TileMeta = {
  id: number;
  position: [number, number];
  value: number;
  mergeWith?: number;
};

type TileProps = {
  // tile value - 2, 4, 8, 16, 32, ..., 2048.∂
  value: number;
  // an array containing the x and y index on the board.
  position: [number, number];
  // the order of tile on the tile stack.
  zIndex: number;
};

export function Tile({ value, position, zIndex }: TileProps) {
  const theme = useTheme();
  const isPortrait = useMediaQuery('(orientation: portrait)');
  const width = isPortrait ? `(100vw - ${theme.spacing(2)})` : `(100vh - ${theme.spacing(12)})`;
  //  state required to animate the highlight
  const [scale, setScale] = React.useState(1);

  // the previous value (prop) - it is undefined if it is a new tile.
  const previousValue = usePrevProps<number>(value);

  // check if tile is within the board boundaries
  const withinBoardBoundaries = position[0] < 4 && position[1] < 4;
  invariant(withinBoardBoundaries, 'Tile out of bound');

  // if it is a new tile...
  const isNew = previousValue === undefined;
  // ...or its value has changed...
  const hasChanged = previousValue !== value;
  // ... then the tile should be highlighted.
  const willHighlight = isNew || hasChanged;

  // useEffect will decide if highlight should be triggered.
  React.useEffect(() => {
    if (willHighlight) {
      setScale(1.1);
      setTimeout(() => setScale(1), 100);
    }
  }, [willHighlight, scale]);

  // all animations come from CSS transition, and we pass them as styles
  const style: React.CSSProperties = React.useMemo(
    () => ({
      top: `calc(${position[1]} / 4 * ${width} + ${theme.spacing(1)})`,
      left: `calc(${position[0]} / 4 * ${width} + ${theme.spacing(1)})`,
      transform: `scale(${scale})`,
      zIndex,
      backgroundColor: darken(
        theme.palette.mode === 'light' ? theme.palette.primary.light : theme.palette.primary.dark,
        Math.log2(value) / Math.log2(2048)
      ),
      color: '#ffffff'
    }),
    [position, scale, zIndex, width, theme, value]
  );

  return React.useMemo(
    () => (
      <StyledTile style={style} value={value}>
        {value}
      </StyledTile>
    ),
    [value, style]
  );
}

type TilePlaceholderProps = Pick<TileProps, 'position'>;

export function TilePlaceholder({ position }: TilePlaceholderProps) {
  const theme = useTheme();
  const isPortrait = useMediaQuery('(orientation: portrait)');
  const width = isPortrait ? `(100vw - ${theme.spacing(2)})` : `(100vh - ${theme.spacing(12)})`;

  // check if tile is within the board boundaries
  const withinBoardBoundaries = position[0] < 4 && position[1] < 4;
  invariant(withinBoardBoundaries, 'Tile out of bound');

  // all animations come from CSS transition, and we pass them as styles
  const style: React.CSSProperties = React.useMemo(
    () => ({
      top: `calc(${position[1]} / 4 * ${width} + ${theme.spacing(1)})`,
      left: `calc(${position[0]} / 4 * ${width} + ${theme.spacing(1)})`,
      backgroundColor: theme.palette.action.disabled
    }),
    [position, width, theme]
  );

  return React.useMemo(() => <StyledTile style={style} value={2} />, [style]);
}
