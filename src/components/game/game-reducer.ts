import { Emitter } from '../../hooks';
import { animationDuration } from '../board';
import { TileMeta } from '../tile';

export type State = {
  tiles: {
    [id: number]: TileMeta;
  };
  byIds: number[];
  hasChanged: boolean;
  inMotion: boolean;
  isInitialRender: boolean;
};

export const initialState: State = {
  tiles: {},
  byIds: [],
  hasChanged: false,
  inMotion: false,
  isInitialRender: true
};

export type Action =
  | { type: 'CREATE_TILE'; tile: TileMeta }
  | { type: 'UPDATE_TILE'; tile: TileMeta }
  | { type: 'MERGE_TILE'; source: TileMeta; destination: TileMeta }
  | { type: 'START_MOVE' }
  | { type: 'END_MOVE' };

export const GameReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'CREATE_TILE':
      const tiles = {
        ...state.tiles,
        [action.tile.id]: action.tile
      };
      const tileMetas = Object.values(tiles)
        .filter((tileMeta) => !!tileMeta)
        .map((tileMeta) => ({
          value: tileMeta.value,
          position: [tileMeta.position[0], tileMeta.position[1]]
        }));
      const isGameOver =
        tileMetas.length >= 16 &&
        tileMetas.filter((tileMeta) => {
          const {
            value,
            position: [x, y]
          } = tileMeta;
          // check if there are tiles that can be merged to this one
          const otherTilesWithSameValues = tileMetas
            // tiles with same value
            .filter((otherTile) => otherTile.value === value)
            // exclude this
            .filter((otherTile) => otherTile.position[0] !== x || otherTile.position[1] !== y);

          const mergableTiles = otherTilesWithSameValues.filter(
            (otherTile) =>
              (otherTile.position[0] === x && (otherTile.position[1] === y - 1 || otherTile.position[1] === y + 1)) ||
              (otherTile.position[1] === y && (otherTile.position[0] === x - 1 || otherTile.position[0] === x + 1))
          );
          // we need tiles that can not be merged
          return mergableTiles.length <= 0;
        }).length >= 16;
      if (isGameOver) {
        setTimeout(() => {
          Emitter.emit('2048-game-over', {
            score: Math.max(...tileMetas.map((tile) => tile.value))
          });
        }, animationDuration * 2);
      }
      return {
        ...state,
        tiles: tiles,
        byIds: [...state.byIds, action.tile.id],
        hasChanged: false
      };
    case 'UPDATE_TILE':
      return {
        ...state,
        tiles: {
          ...state.tiles,
          [action.tile.id]: action.tile
        },
        hasChanged: true
      };
    case 'MERGE_TILE':
      const { [action.source.id]: source, [action.destination.id]: destination, ...restTiles } = state.tiles;
      return {
        ...state,
        tiles: {
          ...restTiles,
          [action.destination.id]: {
            id: action.destination.id,
            value: action.source.value + action.destination.value,
            position: action.destination.position
          }
        },
        byIds: state.byIds.filter((id) => id !== action.source.id),
        hasChanged: true
      };
    case 'START_MOVE':
      return {
        ...state,
        inMotion: true
      };
    case 'END_MOVE':
      return {
        ...state,
        inMotion: false
      };
    default:
      return state;
  }
};
