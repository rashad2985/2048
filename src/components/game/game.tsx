import { BoxProps, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import * as React from 'react';
import { useThrottledCallback } from 'use-debounce';

import { useEvents, useGame, useScores } from '../../hooks';
import { animationDuration, Board } from '../board';

type GameOverProps = {
  isGameOver: boolean;
  score: number;
};

type GameProps = {
  getContainerRef: () => BoxProps<'div', any>['ref'];
};

export function Game({ getContainerRef }: GameProps) {
  const { tileList: tiles, moveLeft, moveRight, moveUp, moveDown, resetGame } = useGame();

  const handleKeyDown = React.useCallback(
    (e: KeyboardEvent) => {
      // disables page scrolling with keyboard arrows
      e?.preventDefault?.();
      e?.stopPropagation?.();

      switch (e.code) {
        case 'ArrowLeft':
          moveLeft();
          break;
        case 'ArrowRight':
          moveRight();
          break;
        case 'ArrowUp':
          moveUp();
          break;
        case 'ArrowDown':
          moveDown();
          break;
      }
    },
    [moveDown, moveUp, moveLeft, moveRight]
  );

  // protects the reducer from being flooded with events.
  const throttledHandleKeyDown = useThrottledCallback(handleKeyDown, animationDuration, {
    leading: true,
    trailing: false
  });

  const startSwipePosition = React.useRef<[number, number]>([0, 0]);
  const cancelTouchPropagation = React.useCallback((touchEvent: TouchEvent) => {
    touchEvent?.stopPropagation?.();
    touchEvent?.preventDefault?.();
  }, []);
  const handleTouchStart = React.useCallback(
    (touchEvent: TouchEvent) => {
      cancelTouchPropagation(touchEvent);
      startSwipePosition.current = [
        touchEvent?.changedTouches?.[0]?.clientX ?? 0,
        touchEvent?.changedTouches?.[0]?.clientY ?? 0
      ];
    },
    [cancelTouchPropagation]
  );
  const handleTouchEnd = React.useCallback(
    (touchEvent: TouchEvent) => {
      cancelTouchPropagation(touchEvent);
      const [startX, startY] = startSwipePosition.current ?? [0, 0];
      const [endX, endY] = [
        touchEvent?.changedTouches?.[0]?.clientX ?? 0,
        touchEvent?.changedTouches?.[0]?.clientY ?? 0
      ];

      if (startX === endX && startY === endY) {
        startSwipePosition.current = [0, 0];
        return;
      }
      const verticalMove = startX - endX;
      const horizontalMove = startY - endY;

      if (Math.abs(verticalMove) < 32 && Math.abs(horizontalMove) < 32) {
        // no swipe, but a touch
        return;
      }

      if (Math.abs(verticalMove) > Math.abs(horizontalMove)) {
        // vertical swipe wins
        if (startX < endX) {
          // swipe right
          moveRight();
        } else {
          // swipe left
          moveLeft();
        }
      } else {
        // horizontal swipe wins
        if (startY < endY) {
          // swipe down
          moveDown();
        } else {
          // swipe up
          moveUp();
        }
      }
    },
    [moveDown, moveLeft, moveRight, moveUp, cancelTouchPropagation]
  );

  const [gameOverProps, setGameOverProps] = React.useState<GameOverProps>({
    isGameOver: false,
    score: 0
  });
  const { add: addScore } = useScores();
  const handleGameOver = React.useCallback(
    (event: Pick<GameOverProps, 'score'>) => {
      addScore(event.score);
      setGameOverProps({ isGameOver: true, score: event.score });
    },
    [addScore]
  );
  const hideGameOverDialog = React.useCallback(
    () => setGameOverProps((prevState) => ({ ...prevState, isGameOver: false })),
    []
  );
  const events = useEvents();

  const restart = React.useCallback(() => {
    events.emit('2048-restart');
    setGameOverProps({ isGameOver: false, score: 0 });
  }, [events]);

  React.useEffect(() => {
    events.on('2048-restart', resetGame);
    return () => {
      events.off('2048-restart', resetGame);
    };
  }, [events, resetGame]);
  React.useEffect(() => {
    events.on('2048-game-over', handleGameOver);
    return () => {
      events.off('2048-game-over', handleGameOver);
    };
  }, [events, handleGameOver]);

  React.useEffect(() => {
    window.addEventListener('keydown', throttledHandleKeyDown);
    return () => {
      window.removeEventListener('keydown', throttledHandleKeyDown);
    };
  }, [throttledHandleKeyDown]);

  React.useEffect(() => {
    const containerRef = getContainerRef();
    containerRef?.addEventListener?.('touchstart', handleTouchStart, false);
    containerRef?.addEventListener?.('touchmove', cancelTouchPropagation, false);
    containerRef?.addEventListener?.('touchend', handleTouchEnd, false);
    containerRef?.addEventListener?.('touchcancel', cancelTouchPropagation, false);

    return () => {
      containerRef?.removeEventListener?.('touchstart', handleTouchStart);
      containerRef?.removeEventListener?.('touchmove', cancelTouchPropagation);
      containerRef?.removeEventListener?.('touchend', handleTouchEnd);
      containerRef?.removeEventListener?.('touchcancel', cancelTouchPropagation);
    };
  }, [getContainerRef, handleTouchStart, handleTouchEnd, cancelTouchPropagation]);

  return React.useMemo(
    () => (
      <React.Fragment>
        <Board tiles={tiles} />
        <Dialog open={gameOverProps.isGameOver} maxWidth={'xs'}>
          <DialogTitle>Game over!</DialogTitle>
          <DialogContent>
            <DialogContentText>Your score: {gameOverProps.score}</DialogContentText>
            <DialogContentText>Would you like to restart the game?</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={hideGameOverDialog}>No</Button>
            <Button onClick={restart}>Yes</Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    ),
    [tiles, gameOverProps.isGameOver, gameOverProps.score, hideGameOverDialog, restart]
  );
}
