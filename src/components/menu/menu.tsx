import { Brightness4, Brightness7, Info, RestartAlt, Score, Translate } from '@mui/icons-material';
import { Box, Divider, List, ListItem, ListItemIcon, ListItemText, SwipeableDrawer, useTheme } from '@mui/material';
import * as React from 'react';
import { FormattedMessage } from 'react-intl';
import { useHistory, useLocation } from 'react-router-dom';
import { useEvents } from '../../hooks';
import { LanguageSelector } from './LanguageSelector';

type Props = {
  visible: boolean;
  hide: () => void;
  show: () => void;
};

export function Menu({ visible, hide, show }: Props) {
  const events = useEvents();
  const history = useHistory();
  const location = useLocation();

  const login = React.useCallback(() => {
    hide();
    history.push({ ...location, pathname: '/scores' });
  }, [hide, location, history]);

  const about = React.useCallback(() => {
    hide();
    history.push({ ...location, pathname: '/about' });
  }, [hide, location, history]);

  const restart = React.useCallback(() => {
    events.emit('2048-restart');
    hide();
  }, [events, hide]);

  const theme = useTheme();
  const togglePalette = React.useCallback(() => {
    events.emit('2048-toggle-palette');
    hide();
  }, [events, hide]);

  return React.useMemo(
    () => (
      <SwipeableDrawer anchor="right" open={visible} onClose={hide} onOpen={show}>
        <Box width={250}>
          <List>
            <ListItem button onClick={restart}>
              <ListItemIcon>
                <RestartAlt />
              </ListItemIcon>
              <ListItemText primary={<FormattedMessage id="menu.restart" />} />
            </ListItem>
            <ListItem button onClick={togglePalette}>
              <ListItemIcon>{theme.palette.mode === 'dark' ? <Brightness7 /> : <Brightness4 />}</ListItemIcon>
              <ListItemText primary={<FormattedMessage id={`menu.use-${theme.palette.mode}-theme`} />} />
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <Translate />
              </ListItemIcon>
              <LanguageSelector />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button onClick={login}>
              <ListItemIcon>
                <Score />
              </ListItemIcon>
              <ListItemText primary={<FormattedMessage id="menu.scores" />} />
            </ListItem>
          </List>
          <Divider />
          <List>
            <ListItem button onClick={about}>
              <ListItemIcon>
                <Info />
              </ListItemIcon>
              <ListItemText primary={<FormattedMessage id="menu.about" />} />
            </ListItem>
          </List>
        </Box>
      </SwipeableDrawer>
    ),
    [visible, hide, show, restart, theme.palette.mode, togglePalette, login, about]
  );
}
