import { ListItemText, MenuItem, TextField, TextFieldProps } from '@mui/material';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { SupportedLocale, SupportedLocales, useLocale } from '../../hooks';

export function LanguageSelector() {
  const { locale, changeLocale } = useLocale();
  const options = React.useMemo(
    () =>
      SupportedLocales.map((locale) => (
        <MenuItem key={locale} value={locale}>
          <ListItemText primary={<FormattedMessage id={`language-selector.locale.${locale}`} />} />
        </MenuItem>
      )),
    []
  );

  const changeLanguage = React.useCallback<Required<TextFieldProps>['onChange']>(
    (event) => changeLocale(event.target.value as SupportedLocale),
    [changeLocale]
  );

  return React.useMemo(
    () => (
      <TextField select fullWidth margin="none" size="small" value={locale} onChange={changeLanguage}>
        {options}
      </TextField>
    ),
    [locale, changeLanguage, options]
  );
}
