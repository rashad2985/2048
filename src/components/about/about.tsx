import { AccessTime, Apps, BuildCircle, Language, Person } from '@mui/icons-material';
import {
  Timeline,
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  TimelineOppositeContent,
  TimelineSeparator
} from '@mui/lab';
import { Link, Typography } from '@mui/material';
import preval from 'preval.macro';
import React from 'react';
import { FormattedDate, FormattedMessage } from 'react-intl';

const buildDateTimeStamp = preval`module.exports = new Date().getTime();` as number;
const buildDateTime = new Date(buildDateTimeStamp);

export function About() {
  return React.useMemo(
    () => (
      <Timeline sx={{ py: '0px', my: '0px' }}>
        <TimelineItem>
          <TimelineOppositeContent sx={{ display: 'none' }} />
          <TimelineSeparator>
            <TimelineConnector sx={{ bgcolor: 'background.paper' }} />
            <TimelineDot color="primary" variant="outlined">
              <Apps color="primary" />
            </TimelineDot>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
          </TimelineSeparator>
          <TimelineContent sx={{ py: '12px', px: 2 }}>
            <Typography variant="h6">
              <FormattedMessage id="about.app-name" />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="about.app-name.label" />
            </Typography>
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineOppositeContent sx={{ display: 'none' }} />
          <TimelineSeparator>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
            <TimelineDot color="primary" variant="outlined">
              <BuildCircle color="primary" />
            </TimelineDot>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
          </TimelineSeparator>
          <TimelineContent sx={{ py: '12px', px: 2 }}>
            <Typography variant="h6">{process.env.REACT_APP_VERSION}</Typography>
            <Typography variant="body2">
              <FormattedMessage id="about.version.label" />
            </Typography>
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineOppositeContent sx={{ display: 'none' }} />
          <TimelineSeparator>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
            <TimelineDot color="primary" variant="outlined">
              <AccessTime color="primary" />
            </TimelineDot>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
          </TimelineSeparator>
          <TimelineContent sx={{ py: '12px', px: 2 }}>
            <Typography variant="h6">
              <FormattedDate
                value={buildDateTime}
                day="numeric"
                month="numeric"
                year="numeric"
                hour="numeric"
                minute="numeric"
              />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="about.build-timestamp.label" />
            </Typography>
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineOppositeContent sx={{ display: 'none' }} />
          <TimelineSeparator>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
            <TimelineDot color="primary" variant="outlined">
              <Person color="primary" />
            </TimelineDot>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
          </TimelineSeparator>
          <TimelineContent sx={{ py: '12px', px: 2 }}>
            <Typography variant="h6">
              <FormattedMessage id="about.responsible-developer" />
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="about.responsible-developer.label" />
            </Typography>
          </TimelineContent>
        </TimelineItem>
        <TimelineItem>
          <TimelineOppositeContent sx={{ display: 'none' }} />
          <TimelineSeparator>
            <TimelineConnector sx={{ bgcolor: 'primary.main' }} />
            <TimelineDot color="primary" variant="outlined">
              <Language color="primary" />
            </TimelineDot>
            <TimelineConnector sx={{ bgcolor: 'background.paper' }} />
          </TimelineSeparator>
          <TimelineContent sx={{ py: '12px', px: 2 }}>
            <Typography variant="h6">
              <Link href="https://rashad.trautmann.dev" target="_blank">
                {`https://rashad.trautmann.dev`}
              </Link>
            </Typography>
            <Typography variant="body2">
              <FormattedMessage id="about.website.label" />
            </Typography>
          </TimelineContent>
        </TimelineItem>
      </Timeline>
    ),
    []
  );
}
