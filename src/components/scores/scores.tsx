import { Flag } from '@mui/icons-material';
import { Grid, List, ListItem, ListItemIcon, ListItemText, Typography } from '@mui/material';
import React from 'react';
import { FormattedDate, FormattedMessage, FormattedNumber } from 'react-intl';
import { useScores } from '../../hooks';

export function Scores() {
  const { get: getScores } = useScores();
  const scores = React.useMemo(
    () =>
      getScores().map((score, index) => (
        <ListItem key={`score-${index}`}>
          <ListItemIcon>{index === 0 && <Flag color="primary" />}</ListItemIcon>
          <ListItemText
            primary={<FormattedNumber value={score.points} />}
            secondary={
              <FormattedDate
                value={score.timestamp}
                day="numeric"
                month="long"
                year="numeric"
                hour="numeric"
                minute="numeric"
              />
            }
          />
        </ListItem>
      )),
    [getScores]
  );

  return React.useMemo(
    () => (
      <Grid container direction="column" justifyContent="flex-start" alignItems="stretch" spacing={2}>
        {scores.length > 0 ? (
          <Grid item xs={12} sm={9} md={6} lg={4} xl={3}>
            <List>{scores}</List>
          </Grid>
        ) : (
          <Grid item xs={12}>
            <Grid container direction="row" justifyContent="center" alignItems="center">
              <Grid item>
                <Typography variant="body1">
                  <FormattedMessage id="scores.no-scores" />
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Grid>
    ),
    [scores]
  );
}
