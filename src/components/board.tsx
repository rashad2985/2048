import { Box, styled } from '@mui/material';
import * as React from 'react';
import { Tile, TileMeta, TilePlaceholder } from './tile';

export const animationDuration = 250;

export const StyledBoard = styled(Box)(({ theme }) => ({
  display: 'flex',
  position: 'relative',
  border: 'none',
  margin: [1, 0],
  padding: theme.spacing(1),
  boxSizing: 'border-box',
  [`${theme.breakpoints.down('sm')}`]: {
    padding: 0
  },
  '@media (orientation: portrait)': {
    minWidth: `calc(100vw - ${theme.spacing(2)})`,
    width: `calc(100vw - ${theme.spacing(2)})`,
    maxWidth: `calc(100vw - ${theme.spacing(2)})`,
    minHeight: `calc(100vw - ${theme.spacing(2)})`,
    height: `calc(100vw - ${theme.spacing(2)})`,
    maxHeight: `calc(100vw - ${theme.spacing(2)})`
  },
  '@media (orientation: landscape)': {
    minWidth: `calc(100vh - ${theme.spacing(12)})`,
    width: `calc(100vh - ${theme.spacing(12)})`,
    maxWidth: `calc(100vh - ${theme.spacing(12)})`,
    minHeight: `calc(100vh - ${theme.spacing(12)})`,
    height: `calc(100vh - ${theme.spacing(12)})`,
    maxHeight: `calc(100vh - ${theme.spacing(12)})`
  }
}));

type Props = {
  tiles: TileMeta[];
};

export function Board({ tiles }: Props) {
  // Render Tile Placeholders
  const tilePlaceholders = React.useMemo(
    () =>
      Array<number[]>(4)
        .fill(Array<number>(4).fill(0))
        .map((row, x) => row.map((_, y) => <TilePlaceholder position={[x, y]} key={`tile-placeholder-${x}-${y}`} />))
        .reduce((previousValue, currentValue) => [...previousValue, ...currentValue], []),
    []
  );

  // render all tiles on the board
  const tileList = tiles.map(({ id, ...restProps }) => <Tile key={`tile-${id}`} {...restProps} zIndex={id} />);

  return React.useMemo(
    () => (
      <StyledBoard>
        <Box width="100%" height="100%" zIndex={0} position="absolute">
          {tilePlaceholders}
        </Box>
        <Box width="100%" height="100%" zIndex={1} position="absolute">
          {tileList}
        </Box>
      </StyledBoard>
    ),
    [tilePlaceholders, tileList]
  );
}
