export type Event = '2048-restart' | '2048-game-over' | '2048-toggle-palette' | '2048-change-language';
