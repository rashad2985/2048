import * as HookState from '@hookstate/core';
import { ArrowBack } from '@mui/icons-material';
import MenuIcon from '@mui/icons-material/Menu';
import {
  AppBar,
  Box,
  createTheme,
  CssBaseline,
  Grid,
  IconButton,
  PaletteMode,
  Paper,
  Slide,
  ThemeProvider,
  Toolbar,
  Typography,
  useMediaQuery
} from '@mui/material';
import React from 'react';
import { IntlProvider } from 'react-intl';
import { HashRouter as Router, Route, Switch, useHistory, useLocation } from 'react-router-dom';
import { About } from './components/about/about';

import { Game, initialState, State } from './components/game';
import { Menu } from './components/menu/menu';
import { Scores } from './components/scores/scores';
import { useEvents, useLocale } from './hooks';

export const GameState = HookState.createState<State>(initialState);

const ContentWithBackButton: React.FC = ({ children }) => {
  const history = useHistory();
  const location = useLocation();
  const [visible, setVisible] = React.useState<boolean>(true);
  const back = React.useCallback(() => {
    setVisible(false);
    history.push({ ...location, pathname: '/' });
  }, [history, location]);
  return React.useMemo(
    () => (
      <Slide direction="right" in={visible}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          spacing={1}
          display="flex"
          flexGrow={1}
          sx={{ py: '16px', px: '16px' }}
        >
          <Grid item xs={12} display="flex">
            <Grid container direction="row" justifyContent="flex-start" alignItems="center">
              <Grid item>
                <IconButton size="large" onClick={back}>
                  <ArrowBack />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12} display="flex" flexGrow={1}>
            <Paper elevation={0} sx={{ display: 'flex', flexGrow: 1 }}>
              {children}
            </Paper>
          </Grid>
        </Grid>
      </Slide>
    ),
    [back, children, visible]
  );
};

export function App() {
  const [menuVisible, setMenuVisibility] = React.useState<boolean>(false);
  const toggleMenu = React.useCallback(() => setMenuVisibility((prevState) => !prevState), []);
  const showMenu = React.useCallback(() => setMenuVisibility(true), []);
  const hideMenu = React.useCallback(() => setMenuVisibility(false), []);

  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const [paletteMode, setPaletteMode] = React.useState<PaletteMode>(prefersDarkMode ? 'dark' : 'light');
  const togglePalette = React.useCallback(
    () => setPaletteMode((prevState) => (prevState === 'light' ? 'dark' : 'light')),
    []
  );
  const theme = React.useMemo(() => createTheme({ palette: { mode: paletteMode } }), [paletteMode]);
  const handlePaletteModeChange = React.useCallback(
    (mediaQueryListEvent: MediaQueryListEvent) => setPaletteMode(mediaQueryListEvent.matches ? 'dark' : 'light'),
    []
  );
  React.useEffect(() => {
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', handlePaletteModeChange);
    return () => {
      window.matchMedia('(prefers-color-scheme: dark)').removeEventListener('change', handlePaletteModeChange);
    };
  });

  const events = useEvents();
  React.useEffect(() => {
    events.on('2048-toggle-palette', togglePalette);
    return () => {
      events.off('2048-toggle-palette', togglePalette);
    };
  }, [events, togglePalette]);

  const containerRef = React.useRef(null);
  const getContainerRef = React.useCallback(() => containerRef.current, [containerRef]);

  const { locale, messages } = useLocale();
  return React.useMemo(
    () => (
      <IntlProvider locale={locale} messages={messages} defaultLocale="en">
        <Router basename="ui">
          <CssBaseline />
          <ThemeProvider theme={theme}>
            <Box
              width="100vw"
              height="100vh"
              display="flex"
              flexDirection="column"
              justifyContent="flex-start"
              alignItems="center"
              bgcolor="background.default"
            >
              <AppBar position="fixed">
                <Toolbar>
                  <Grid container direction="row" justifyContent="space-between" alignItems="center" spacing={1}>
                    <Grid item>
                      <Typography variant="h6" noWrap component="div">
                        2048
                      </Typography>
                    </Grid>
                    <Grid item>
                      <IconButton size="large" edge="end" color="inherit" aria-label="menu" onClick={toggleMenu}>
                        <MenuIcon />
                      </IconButton>
                    </Grid>
                  </Grid>
                </Toolbar>
              </AppBar>
              <Menu visible={menuVisible} hide={hideMenu} show={showMenu} />
              <Grid
                ref={containerRef}
                container
                direction="column"
                justifyContent="flex-start"
                alignItems="stretch"
                flexGrow={1}
              >
                <Grid item xs={12} flexGrow={1} display="flex">
                  <Box
                    display="flex"
                    flexDirection="row"
                    justifyContent="center"
                    alignItems="flex-start"
                    marginTop={6}
                    paddingTop={2}
                    flexGrow={1}
                  >
                    <Switch>
                      <Route path="/" exact>
                        <Game getContainerRef={getContainerRef} />
                      </Route>
                      <Route path="/scores" exact>
                        <ContentWithBackButton>
                          <Scores />
                        </ContentWithBackButton>
                      </Route>
                      <Route path="/about" exact>
                        <ContentWithBackButton>
                          <About />
                        </ContentWithBackButton>
                      </Route>
                      <Route>
                        <ContentWithBackButton>404</ContentWithBackButton>
                      </Route>
                    </Switch>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </ThemeProvider>
        </Router>
      </IntlProvider>
    ),
    [locale, messages, theme, menuVisible, showMenu, hideMenu, toggleMenu, getContainerRef]
  );
}
