import EventEmitter from 'eventemitter3';
import React from 'react';
import { Event } from '../event';

const eventEmitter = new EventEmitter();

type EmitterFunction = (event: Event, callback: (...args: any[]) => void) => EventEmitter<string | symbol>;

type EmitterProps = {
  on: EmitterFunction;
  once: EmitterFunction;
  off: EmitterFunction;
  emit: (event: Event, ...args: any[]) => boolean;
};

const Emitter: EmitterProps = {
  on: (event, callback) => eventEmitter.on(event, callback),
  once: (event, callback) => eventEmitter.once(event, callback),
  off: (event, callback) => eventEmitter.off(event, callback),
  emit: (event, payload) => eventEmitter.emit(event, payload)
};

Object.freeze(Emitter);

export { Emitter };

export function useEvents(): EmitterProps {
  return React.useMemo(() => Emitter, []);
}
