import React from 'react';
import { az, de, en, ru } from '../translations';
import { useEvents } from './use-events';

const Locale = 'locale';

const getLocaleFromStore = () => localStorage.getItem(Locale) ?? '';
const setLocaleInStore = (locale: SupportedLocale | undefined) => localStorage.setItem(Locale, locale ?? '');

export type SupportedLocale = 'en' | 'de' | 'ru' | 'az';

export const SupportedLocales: SupportedLocale[] = ['en', 'de', 'ru', 'az'];

export function useLocale() {
  const { emit, on, off } = useEvents();
  const resolveLocale = React.useCallback(
    () =>
      [getLocaleFromStore(), ...navigator.languages]
        .map((language) => language.substring(0, 2).toLowerCase())
        .filter((locale) => SupportedLocales.includes(locale as SupportedLocale))
        .map((locale) => locale as SupportedLocale)?.[0] ?? 'en',
    []
  );

  const [locale, setLocale] = React.useState<SupportedLocale>(resolveLocale());

  const messages = React.useMemo<Record<string, string>>(
    () =>
      (locale === 'en' && en) || (locale === 'de' && de) || (locale === 'ru' && ru) || (locale === 'az' && az) || en,
    [locale]
  );

  const changeLocale = React.useCallback(
    (newLocale: SupportedLocale) => {
      setLocaleInStore(newLocale);
      emit('2048-change-language');
    },
    [emit]
  );

  const updateLocale = React.useCallback(() => setLocale(resolveLocale()), [resolveLocale]);
  React.useEffect(() => {
    on('2048-change-language', updateLocale);
    return () => {
      off('2048-change-language', updateLocale);
    };
  }, [on, off, updateLocale]);

  return React.useMemo(() => ({ locale, messages, changeLocale }), [locale, messages, changeLocale]);
}
