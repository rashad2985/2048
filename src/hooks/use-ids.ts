let seqId = 1;

/**
 * Returns next sequential number.
 */
export function useIds() {
  const nextId = () => {
    return seqId++;
  };

  return [nextId];
}
