import * as HookState from '@hookstate/core';
import * as React from 'react';
import { GameState } from '../app';
import { animationDuration } from '../components/board';
import { Action, GameReducer, initialState } from '../components/game';
import { TileMeta } from '../components/tile';
import { useIds } from './use-ids';

type RetrieveTileIdsPerRowOrColumn = (rowOrColumnIndex: number) => number[];

type CalculateTileIndex = (
  tileIndex: number,
  tileInRowIndex: number,
  howManyMerges: number,
  maxIndexInRow: number
) => number;

export function useGame() {
  const [nextId] = useIds();
  // state
  const { get: getState, set: setState } = HookState.useState(GameState);
  const dispatch = React.useCallback(
    (action: Action) => setState((prevState) => GameReducer(prevState, action)),
    [setState]
  );

  const { tiles, byIds, hasChanged, inMotion, isInitialRender } = getState();

  const createTile = React.useCallback(
    ({ position, value }: Partial<TileMeta>) => {
      const tile = {
        id: nextId(),
        position,
        value
      } as TileMeta;
      dispatch({ type: 'CREATE_TILE', tile });
    },
    [nextId, dispatch]
  );

  const mergeTile = React.useCallback(
    (source: TileMeta, destination: TileMeta) => dispatch({ type: 'MERGE_TILE', source, destination }),
    [dispatch]
  );

  // A must-have to keep the sliding animation if the action merges tiles together.
  const throttledMergeTile = React.useCallback(
    (source: TileMeta, destination: TileMeta) => {
      setTimeout(() => mergeTile(source, destination), animationDuration);
    },
    [mergeTile]
  );

  const updateTile = React.useCallback((tile: TileMeta) => dispatch({ type: 'UPDATE_TILE', tile }), [dispatch]);

  const didTileMove = React.useCallback(
    (source: TileMeta, destination: TileMeta) =>
      source.position[0] !== destination.position[0] || source.position[1] !== destination.position[1],
    []
  );

  const positionToIndex = React.useCallback((position: [number, number]) => position[1] * 4 + position[0], []);

  const indexToPosition = React.useCallback((index: number) => [index % 4, Math.floor(index / 4)], []);

  const retrieveTileMap = React.useCallback(() => {
    const tileMap = Array(16).fill(0) as number[];

    byIds.forEach((id) => {
      const { position } = tiles[id];
      const index = positionToIndex(position);
      tileMap[index] = id;
    });

    return tileMap;
  }, [byIds, tiles, positionToIndex]);

  const findEmptyTiles = React.useCallback(() => {
    const tileMap = retrieveTileMap();

    const emptyTiles = tileMap.reduce((result, tileId, index) => {
      if (tileId === 0) {
        return [...result, indexToPosition(index) as [number, number]];
      }

      return result;
    }, [] as [number, number][]);

    return emptyTiles;
  }, [retrieveTileMap, indexToPosition]);

  const generateRandomTile = React.useCallback(() => {
    const emptyTiles = findEmptyTiles();

    if (emptyTiles.length > 0) {
      const index = Math.floor(Math.random() * emptyTiles.length);
      const position = emptyTiles[index];

      createTile({ position, value: 2 });
    }
  }, [findEmptyTiles, createTile]);

  const move = React.useCallback(
    (retrieveTileIdsPerRowOrColumn: RetrieveTileIdsPerRowOrColumn, calculateFirstFreeIndex: CalculateTileIndex) => {
      // new tiles cannot be created during motion.
      dispatch({ type: 'START_MOVE' });

      const maxIndex = 4 - 1;

      // iterates through every row or column (depends on move kind - vertical or horizontal).
      for (let rowOrColumnIndex = 0; rowOrColumnIndex < 4; rowOrColumnIndex += 1) {
        // retrieves tiles in the row or column.
        const availableTileIds = retrieveTileIdsPerRowOrColumn(rowOrColumnIndex);

        // previousTile is used to determine if tile can be merged with the current tile.
        let previousTile: TileMeta | undefined;
        // mergeCount helps to fill gaps created by tile merges - two tiles become one.
        let mergedTilesCount = 0;

        // interate through available tiles.
        availableTileIds.forEach((tileId, nonEmptyTileIndex) => {
          const currentTile = tiles[tileId];

          // if previous tile has the same value as the current one they should be merged together.
          if (previousTile !== undefined && previousTile.value === currentTile.value) {
            const tile = {
              ...currentTile,
              position: previousTile.position,
              mergeWith: previousTile.id
            } as TileMeta;

            // delays the merge by 250ms, so the sliding animation can be completed.
            throttledMergeTile(tile, previousTile);
            // previous tile must be cleared as a single tile can be merged only once per move.
            previousTile = undefined;
            // increment the merged counter to correct position for the consecutive tiles to get rid of gaps
            mergedTilesCount += 1;

            return updateTile(tile);
          }

          // else - previous and current tiles are different - move the tile to the first free space.
          const tile = {
            ...currentTile,
            position: indexToPosition(
              calculateFirstFreeIndex(rowOrColumnIndex, nonEmptyTileIndex, mergedTilesCount, maxIndex)
            )
          } as TileMeta;

          // previous tile become the current tile to check if the next tile can be merged with this one.
          previousTile = tile;

          // only if tile has changed its position it will be updated
          if (didTileMove(currentTile, tile)) {
            return updateTile(tile);
          }
        });
      }

      // wait until the end of all animations.
      setTimeout(() => dispatch({ type: 'END_MOVE' }), animationDuration);
    },
    [didTileMove, indexToPosition, throttledMergeTile, tiles, updateTile, dispatch]
  );

  const moveLeftFactory = React.useCallback(() => {
    const retrieveTileIdsByRow = (rowIndex: number) => {
      const tileMap = retrieveTileMap();

      const tileIdsInRow = [
        tileMap[rowIndex * 4 + 0],
        tileMap[rowIndex * 4 + 1],
        tileMap[rowIndex * 4 + 2],
        tileMap[rowIndex * 4 + 3]
      ];

      const nonEmptyTiles = tileIdsInRow.filter((id) => id !== 0);
      return nonEmptyTiles;
    };

    const calculateFirstFreeIndex = (tileIndex: number, tileInRowIndex: number, howManyMerges: number, _: number) => {
      return tileIndex * 4 + tileInRowIndex - howManyMerges;
    };

    return () => move(retrieveTileIdsByRow, calculateFirstFreeIndex);
  }, [move, retrieveTileMap]);

  const moveRightFactory = React.useCallback(() => {
    const retrieveTileIdsByRow = (rowIndex: number) => {
      const tileMap = retrieveTileMap();

      const tileIdsInRow = [
        tileMap[rowIndex * 4 + 0],
        tileMap[rowIndex * 4 + 1],
        tileMap[rowIndex * 4 + 2],
        tileMap[rowIndex * 4 + 3]
      ];

      const nonEmptyTiles = tileIdsInRow.filter((id) => id !== 0);
      return nonEmptyTiles.reverse();
    };

    const calculateFirstFreeIndex = (
      tileIndex: number,
      tileInRowIndex: number,
      howManyMerges: number,
      maxIndexInRow: number
    ) => {
      return tileIndex * 4 + maxIndexInRow + howManyMerges - tileInRowIndex;
    };

    return () => move(retrieveTileIdsByRow, calculateFirstFreeIndex);
  }, [move, retrieveTileMap]);

  const moveUpFactory = React.useCallback(() => {
    const retrieveTileIdsByColumn = (columnIndex: number) => {
      const tileMap = retrieveTileMap();

      const tileIdsInColumn = [
        tileMap[columnIndex + 4 * 0],
        tileMap[columnIndex + 4 * 1],
        tileMap[columnIndex + 4 * 2],
        tileMap[columnIndex + 4 * 3]
      ];

      const nonEmptyTiles = tileIdsInColumn.filter((id) => id !== 0);
      return nonEmptyTiles;
    };

    const calculateFirstFreeIndex = (
      tileIndex: number,
      tileInColumnIndex: number,
      howManyMerges: number,
      _: number
    ) => {
      return tileIndex + 4 * (tileInColumnIndex - howManyMerges);
    };

    return () => move(retrieveTileIdsByColumn, calculateFirstFreeIndex);
  }, [move, retrieveTileMap]);

  const moveDownFactory = React.useCallback(() => {
    const retrieveTileIdsByColumn = (columnIndex: number) => {
      const tileMap = retrieveTileMap();

      const tileIdsInColumn = [
        tileMap[columnIndex + 4 * 0],
        tileMap[columnIndex + 4 * 1],
        tileMap[columnIndex + 4 * 2],
        tileMap[columnIndex + 4 * 3]
      ];

      const nonEmptyTiles = tileIdsInColumn.filter((id) => id !== 0);
      return nonEmptyTiles.reverse();
    };

    const calculateFirstFreeIndex = (
      tileIndex: number,
      tileInColumnIndex: number,
      howManyMerges: number,
      maxIndexInColumn: number
    ) => {
      return tileIndex + 4 * (maxIndexInColumn - tileInColumnIndex + howManyMerges);
    };

    return () => move(retrieveTileIdsByColumn, calculateFirstFreeIndex);
  }, [move, retrieveTileMap]);

  const resetGame = React.useCallback(() => {
    setState(initialState);
    createTile({ position: [0, 1], value: 2 });
    createTile({ position: [0, 2], value: 2 });
    setState((prevState) => ({ ...prevState, isInitialRender: false }));
  }, [setState, createTile]);

  React.useEffect(() => {
    if (isInitialRender) {
      resetGame();
      return;
    }

    if (!inMotion && hasChanged) {
      generateRandomTile();
    }
  }, [hasChanged, inMotion, createTile, generateRandomTile, isInitialRender, resetGame]);

  const tileList = byIds.map((tileId) => tiles[tileId]);

  const moveLeft = moveLeftFactory();
  const moveRight = moveRightFactory();
  const moveUp = moveUpFactory();
  const moveDown = moveDownFactory();

  return React.useMemo<{
    tileList: TileMeta[];
    moveLeft: () => void;
    moveRight: () => void;
    moveUp: () => void;
    moveDown: () => void;
    resetGame: () => void;
  }>(
    () => ({ tileList, moveLeft, moveRight, moveUp, moveDown, resetGame }),
    [tileList, moveLeft, moveRight, moveUp, moveDown, resetGame]
  );
}
