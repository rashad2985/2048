import React from 'react';

const Scores = 'scores';

export type Score = {
  points: number;
  timestamp: Date;
};

const parseScore = (key: string, value: any) => {
  if (key === 'points') {
    return Number(value);
  }
  if (key === 'timestamp') {
    return new Date(value);
  }
  return value;
};

const compareScores = (a: Score, b: Score) => b.points - a.points;

const getScores = () =>
  ((JSON.parse(localStorage.getItem(Scores) ?? '[]', parseScore) ?? []) as Score[]).sort(compareScores);

const setScores = (scores: Score[] = []) => localStorage.setItem(Scores, JSON.stringify(scores.sort(compareScores)));

export function useScores() {
  const get = React.useCallback(getScores, []);
  const add = React.useCallback(
    (score: number) => {
      const scores: Score[] = [...get(), { points: score, timestamp: new Date() }].sort(compareScores);
      setScores(scores);
    },
    [get]
  );

  return React.useMemo(() => ({ get, add }), [get, add]);
}
