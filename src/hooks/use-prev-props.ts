import * as React from 'react';

/**
 * `usePrevProps` stores the previous value of the prop.
 *
 * @link https://blog.logrocket.com/how-to-get-previous-props-state-with-react-hooks/
 * @param {T} value
 * @returns {T | undefined}
 */
export function usePrevProps<T = any>(value: T) {
  const ref = React.useRef<T>();

  React.useEffect(() => {
    ref.current = value;
  });

  return ref.current;
}
